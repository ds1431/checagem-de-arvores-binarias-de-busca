# Aula 26/09/2023 - Checagem de Árvores Binárias de Busca

### Enunciado

Implemente uma função que verifica se uma dada árvore é ou não uma árvore binária de busca.

int arv_bin_check(Arvore * a);
