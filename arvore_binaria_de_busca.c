#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Definição da estrutura de um nó de árvore
typedef struct TreeNode {
    int valor;
    struct TreeNode *esquerda;
    struct TreeNode *direita;
} Arvore;

// Função auxiliar recursiva para verificar se uma árvore é uma BST
bool arv_bin_check_recursiva(Arvore *raiz, int *min, int *max) {
    // Caso base: se a árvore estiver vazia, é uma BST
    if (raiz == NULL) {
        return true;
    }

    // Verifica se o valor atual do nó está dentro do intervalo válido
    if ((min != NULL && raiz->valor <= *min) || (max != NULL && raiz->valor >= *max)) {
        return false;
    }

    // Recursivamente verifica as subárvores esquerda e direita
    return (arv_bin_check_recursiva(raiz->esquerda, min, &(raiz->valor)) &&
            arv_bin_check_recursiva(raiz->direita, &(raiz->valor), max));
}

// Função principal para verificar se uma árvore é uma BST
bool arv_bin_check(Arvore *arvore) {
    // Chamada inicial com intervalos mínimos e máximos indefinidos
    return arv_bin_check_recursiva(arvore, NULL, NULL);
}

int main() {
    // Exemplo de uma árvore binária de busca
    Arvore *arvore = malloc(sizeof(Arvore));
    arvore->valor = 10;
    arvore->esquerda = malloc(sizeof(Arvore));
    arvore->esquerda->valor = 5;
    arvore->esquerda->esquerda = NULL;
    arvore->esquerda->direita = NULL;
    arvore->direita = malloc(sizeof(Arvore));
    arvore->direita->valor = 15;
    arvore->direita->esquerda = NULL;
    arvore->direita->direita = NULL;

    // Verifica se a árvore é uma BST
    if (arv_bin_check(arvore)) {
        printf("A arvore eh uma arvore binaria de busca.\n");
    } else {
        printf("A arvore NAO eh uma arvore binaria de busca.\n");
    }

    // Libera a memória da árvore
    free(arvore->esquerda);
    free(arvore->direita);
    free(arvore);

    return 0;
}
